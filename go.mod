module jPolly

go 1.17

require (
	github.com/gorilla/mux v1.5.0
	github.com/jinzhu/gorm v1.9.16
	github.com/nanobox-io/golang-scribble v0.0.0-20170327214430-da3a159a4622
	github.com/pkg/errors v0.9.1
	github.com/skratchdot/open-golang v0.0.0-20160302144031-75fb7ed4208c
)

require (
	github.com/gorilla/context v1.1.1 // indirect
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
)
